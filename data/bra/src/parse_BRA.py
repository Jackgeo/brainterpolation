#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jan 19 09:25:38 2021

@author: beilin
"""
import xmltodict
import base64

import gpsdatetime as g

t = g.gpsdatetime()


t -= 86400 * 25 

WD = "../"
massifs = ["Aravis", "Mont-Blanc"]



for massif in massifs:
    file = "../%4d/%03d/BRA_%s_%4d-%02d-%02d.xml" % (t.yyyy, t.doy, massif, t.yyyy, t.mon, t.dd)
    print(file)
    
    try:
    
        with open(file) as f:
            str_result = f.read()
        f.close()
                
        data = xmltodict.parse(str_result)
        
        s = data["BULLETINS_NEIGE_AVALANCHE"]["@DATEVALIDITE"]
        print(s) 
        
        s = data["BULLETINS_NEIGE_AVALANCHE"]["CARTOUCHERISQUE"]["RISQUE"]
        print(s["@RISQUE1"]) 
    except:
        print("Arrrrgh ! ")