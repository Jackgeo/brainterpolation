# -*- coding: utf-8 -*-
import requests
import re
from bs4 import BeautifulSoup
import gpsdatetime as g
import os
import csv

class xmlBra():
    def __init__(self, text):
        self.text = text
        self.parseXml()
    
    def parseXml(self):
#        with open(self.filename, 'rt') as xml_file:
#            xml = xml_file.read()
        soup = BeautifulSoup(self.text, "lxml")
        result = soup.find('bulletins_neige_avalanche').find('datevalidite').text
        self.datevalidite = g.gpsdatetime(iso=result)
        result = soup.find('bulletins_neige_avalanche').get('massif')
        self.massif = re.sub('/', '-', result)
        
class zoneBra():
    def __init__(self, zone, regroupement, dept, groupe, region, opp):
        self.zone = zone
        self.regroupement = regroupement
        self.dept = dept
        self.groupe = groupe
        self.region = region
        self.opp = int(opp)

    def __str__(self):
        s = ''
        s += "Zone : %-25s %02d" % (self.zone, int(self.opp))
        return s
    
    def getBraXml(self):
        
        url0 = 'https://meteofrance.com/'        
        r = requests.get(url0)
        
        Session_cookies = r.raw.headers["Set-cookie"]
        Session_cookies = re.sub('mfsession=', '', Session_cookies)
        Tab = Session_cookies.split(';')
        Session_cookies = Tab[0]
        #print(Session_cookies)
        myToken = shiftAscii(Session_cookies, 13)
        
        L =[]
        zones= [10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 40, 41, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74]
        for zone in zones:
            
            urlBra = 'https://rpcache-aa.meteofrance.com/internet2018client/2.0/report?domain=%d&report_type=Forecast&report_subtype=BRA' % zone
            head = {'Authorization': 'Bearer %s' % myToken}
            r = requests.get(urlBra, headers=head)
            
            if re.search('"error":404', r.text):
                print("Error 404 : %02d" % zone)
                continue
            
            try:
                bra = xmlBra(r.text)  
            except Exception as e:
                print("Error : unable to parse BRA data for zone %02d" % zone, e)
                continue
                
            try:
                print("Info : BRA %s zone %02d" % (bra.massif, zone))
                L.append(zone)
                filename = 'BRA_%s_%s.xml' % (bra.massif, bra.datevalidite.st_iso_epoch(0))
                with open(filename, 'wt') as f:
                    f.write(r.text)
            except Exception as e:
                print("Error : unable to save BRA for zone %02d" % zone, e)
        
        print(L)        
    
class dept():
    def __init__(self, num, region, groupe, DossierStockage):
        self.DossierStockage = DossierStockage
        self.num = num
        self.region = region
        self.groupe = groupe
        self.ListBra = []
        
def LoadListeDept(file):
    L = []
    with open(file, newline='') as csvfile:
        spamreader = csv.reader(csvfile, delimiter=',', quotechar='"')
        for r in spamreader:
            L.append({'num' : r[0], 'region' : r[1], 'groupe' : r[2]})
    return L
        
class lisBRA():
    def __init__(self, file):
        self.file = file
        self.LoadListeBra()
               
    def LoadListeBra(self):
        L = []
        with open(self.file, newline='') as csvfile:
            spamreader = csv.reader(csvfile, delimiter=';', quotechar='"')
            for r in spamreader:
                L.append(zoneBra(r[0], r[1], r[2], r[3], r[4], r[5]))
        self.L = L
        return L
    
    def getBraXml(self, dataDir = ''):
        
        url0 = 'https://meteofrance.com/'       
        r = requests.get(url0)
        
        Session_cookies = r.raw.headers["Set-cookie"]
        Session_cookies = re.sub('mfsession=', '', Session_cookies)
        Tab = Session_cookies.split(';')
        Session_cookies = Tab[0]
        #print(Session_cookies)
        myToken = shiftAscii(Session_cookies, 13)
        
        t = g.gpsdatetime()
        if t.hh > 15:
            t += 86400
        date = "%04d-%02d-%02d" % (t.yyyy, t.mon, t.dd) 

        fullDataDir = os.path.join(dataDir, "%04d" % t.yyyy, "%03d" % t.doy)
        if os.path.exists(fullDataDir) == False:
            try:
                os.mkdir(os.path.join(os.path.join(dataDir, "%04d" % t.yyyy)))
            except:
                print("Unable to create " + os.path.join(os.path.join(dataDir, "%04d" % t.yyyy)))
            try:
                os.mkdir(os.path.join(dataDir, "%04d" % t.yyyy, "%03d" % t.doy))
            except:
                print("Unable to create " + os.path.join(dataDir, "%04d" % t.yyyy, "%03d" % t.doy))
           
        
        L =[]
        for bra in self.L:

            zone = bra.opp
            filename = os.path.join(fullDataDir, 'BRA_%s_%s.xml' % (re.sub(' ', '-',bra.zone), date))
            
            toBeGet = True
            try:
                if os.path.exists(filename):
                    st = os.stat(filename)
                    if st.st_size > 1e3:
                        toBeGet = False
            except Exception as e:
                print("Error : unable to get stats for %s" % filename, e)
                
            if toBeGet == False:
                print("Info : BRA already downloaded %s" % filename)
                continue

            print(bra)
            
            urlBra = 'https://rpcache-aa.meteofrance.com/internet2018client/2.0/report?domain=%02d&report_type=Forecast&report_subtype=BRA' % zone
            head = {'Authorization': 'Bearer %s' % myToken}
            r = requests.get(urlBra, headers=head)
            
            if re.search('"error":404', r.text):
                print("Error 404 : %02d" % zone)
                continue
            
            try:
                bra = xmlBra(r.text)  
            except Exception as e:
                print("Error : unable to parse BRA data for zone %s" % bra.zone, e)
                continue
                            
            try:
                if toBeGet:
                    print("Info : saving BRA %s zone %02d" % (bra.massif, zone))
                    L.append(zone)
                    with open(filename, 'wt') as f:
                        f.write(r.text)
            
            except Exception as e:
                print("Error : unable to save BRA for zone %s" % bra.massif, e)
                
        
        print(L)

def shiftAscii(sIN, offset = 0):
    """
    shifts string by offset characters 
    """
    sOUT = ""
    for s in sIN:
        if re.search(s, '0123456789._-'):
            sOUT += s
        else:
            c = ord(s)
            if c < 97:
                c += offset
                if c > 90:
                    c -= 26
            else:
                c += offset
                if c > 122:
                    c -= 26
            sOUT += chr(c)
            
    return sOUT


if __name__ == "__main__":

    t1 = g.gpsdatetime()

    WD = os.path.dirname(os.path.abspath(__file__))
#    print(WD)

    DossierStockage = "/home/pub/bra"

    LD = LoadListeDept(os.path.join(WD, "Liste_Dept_Bra.csv"))
#    print(LD)
    
    ListBra = lisBRA(os.path.join(WD,'liste_BRA.csv'))
    for l in ListBra.L:
        print(l)
#    ListBra.getBraXml(DossierStockage)
    
    t2 = g.gpsdatetime()
    print('%.3f sec elapsed ' % (t2-t1))
