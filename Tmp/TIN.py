#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jan 18 16:10:22 2021

@author: obb
"""
#Jacqueline Williams - 21/01/2020
#Interpolation BRA 
#Python 3.8.5 - QGIS 3.16

#importing modules os, qgis.core, and re 
import os
import qgis.core
import re
import numpy as np
from fuzzywuzzy import fuzz,process

import xmltodict
import base64

import gpsdatetime as g


BaseDir= ('/home/obb/Desktop/TINplugin')
TmpDir=(BaseDir+'/Tmp/')

FrBRA=(BaseDir+'/FranceBRA/Massifs_BRA.shp')
AlpesBRA=(BaseDir+'/MassifBRA/Alpes/AlpesBRA.shp')
PyreneesBRA=(BaseDir+'/MassifBRA/Pyrenees/Pyrenees.shp')
CorseBRA=(BaseDir+'/MassifBRA/Corse/Corse.shp')
JuraVosgesBRA=(BaseDir+'/MassifBRA/JuraVosges/JuraVosges.shp')
MCentralBRA=(BaseDir+'/MassifBRA/MassifCentral/Massifcentral.shp')


################################################################################3

###Bring in the date factor, the second line allows us to choose a date other than today
###the first integer is the number of seconds in a day, the second integer is the number of days behind hence the "-='
###thedate variable provides us with the date we have chosen
t = g.gpsdatetime() 
t -= 86400 * 7
thedate = "%02d-%02d-%4d" % (t.dd, t.mon,t.yyyy )
thedate=str(thedate)

#!!!! you haven't provided these variables yet to pluging
dataDir=BaseDir+'/bra/'
fileDir = dataDir+"%4d/%03d/" % (t.yyyy, t.doy)
######################################################################
#clearDir clears our Tmp file for a fresh start
def clearDir(L):
    for f in os.listdir(L):
        os.remove(os.path.join(L, f))
        
def LoadVector (filename, nom="vector"):
    layer = QgsVectorLayer(filename, nom)
    if not layer.isValid():
        print("Layer failed to load!")
    else:
        QgsProject.instance().addMapLayer(layer)
        
    return layer
   
#creates a point grid that covers a fixed extent. See below. It would be interesting to change from a fixed extent.
#It takes a distance Horizontal and vertical parameters in meters which indicate where to plot each knot.
        
    
def LoadRaster(layer,n='massif'):
    layer = QgsRasterLayer(layer, n)
    if layer.isValid():
        QgsProject.instance().addMapLayer(layer)
        
    return layer 

def match_term(term,list_names,min_score=0):
    max_score = -1
    max_name = '' 
    for term2 in list_names:
        score = fuzz.ratio(term,term2)
        if (score>min_score) & (score>max_score):
            max_name = term2
            max_score = score  
    return(max_name,max_score)

#########################################################################################        
 ##################################################################################
###fuzzywuzzy
#################################################################################


LoadVector(FrBRA, 'FrBRA')  

L = os.listdir(fileDir)

layer = QgsVectorLayer(FrBRA, 'FrBRA',"ogr")
layerFields = layer.fields().names()
indexFileID = layerFields.index('fileID')  
indexBRA = layerFields.index('BRA')
        

features = layer.getFeatures()

for feat in features:

        inAttr = feat.attributes() # Input attributes
        massif = (inAttr[indexFileID])
        expectedFile = "BRA_%s_%4d-%02d-%02d.xml" % (massif, t.yyyy, t.mon, t.dd)
        trueFile, ratio = match_term(expectedFile,L,95)
        
        file = os.path.join(fileDir, trueFile)
        
      
        try:
       
            with open(file) as f:
                str_result = f.read()
            f.close()
                    
            data = xmltodict.parse(str_result)
    ###Datevalidite confirms to us that we are indeed pulling values of the correct date    
            s = data["BULLETINS_NEIGE_AVALANCHE"]["@DATEVALIDITE"]
            print(s) 
    ###Risque1 provides us with our BRA value        
            s = data["BULLETINS_NEIGE_AVALANCHE"]["CARTOUCHERISQUE"]["RISQUE"]
            print(s["@RISQUE1"])        
            Danger = int(s["@RISQUE1"])
                      
        except:
            Danger = None
        
        layer_provider = layer.dataProvider()
        layer.startEditing()
        id=feat.id()                      
        attr_value={indexBRA:Danger}
        layer_provider.changeAttributeValues({id:attr_value})
        layer.commitChanges()
            

        
        
#        feat['BRA']= Danger
#        layer.AddFeatures( feat['BRA'])
#        layer.commit
        
    
#        indexBRA.setfeatures(Danger)

        
        
#        
##print(listBRA)
##
##
##
#####Number of zones in our current national shapefile, should be 56 but may change if the shapefile changes
##listBRACount = len(listBRA)
#####Number of BRAzones in our national shapefile that are left with no value
##nanCount = listBRA.count(np.nan)
#####number of BRA values we collected in our listBRA for that day, 
#####these BRA values will be added as attributes in a BRA feature in order to continue our interpolations
##valueCount = listBRAcount-nvCount
#####This counts the files that Yeti collected from Meteo France, we should be getting one value per file
##BRACount = len(os.listdir(fileDir))
#
#
#
####Here we are checking if we are missing any values for said day. We either get a success or an error message.
#if valueCount==BRACount:
#    BRACount = str(BRACount)
#    print("Congratulations, All "+BRACount+" of BRA values of "+thedate+" were retrieved!")
#else:
#    missingV= BRACount-valueCount
#    missingV=str(missingV)
#    print("Warning : Missing "+missingV+" BRA values for "+thedate)


#######################################################################################################
##########################################################################################################

#
    
######################################################################################################

#
#
#def ExtentStr():    
#    alayer = iface.activeLayer()
#    ext = alayer.extent()
#    
#    xmin = ext.xMinimum()
#    xmax = ext.xMaximum()
#    ymin = ext.yMinimum()
#    ymax = ext.yMaximum()
#    Ext = '%f,%f,%f,%f [EPSG:2154]'%(xmin,xmax,ymin,ymax) 
#    print(Ext)
#    
#    return Ext
#
#
#def Triangulation(layer,Extent):
#    
#    print(Extent)
# 
#    parameters = {'INTERPOLATION_DATA':layer+'::~::0::~::6::~::0','METHOD':0,'EXTENT': Extent,'PIXEL_SIZE':100.00,'OUTPUT': TmpDir+'output.tif'}
#    print(parameters)
#    processing.run("qgis:tininterpolation", parameters )            
#
#    parameters = {'INPUT': TmpDir+'output.tif','MASK': layer ,'OUTPUT': TmpDir+'outputclip.tif'}
#    processing.run("gdal:cliprasterbymasklayer", parameters) 
#    
#    parameters = {'INPUT': TmpDir+'outputclip.tif' ,'BAND':1,'COMPUTE_EDGES':False,'COLOR_TABLE': BaseDir+'/BRAcolor.txt','MATCH_MODE':2,'OPTIONS':'','EXTRA':'','OUTPUT': TmpDir+'outputclr.tif'}
#    processing.run("gdal:colorrelief", parameters)
#    

    
################################################################################
## Nettoyage du projet Qgis
#QgsProject.instance().removeAllMapLayers()
## Recuperation de la racine du projet
#root = QgsProject.instance().layerTreeRoot ()
## Suppression des anciens noeuds
#root.removeAllChildren()
#
#
#clearDir(TmpDir)
################################################################################
#
#layer = AlpesBRA
#
#LoadVector(layer)
#Extent = ExtentStr()
#print(Extent)
#Triangulation(layer,Extent)
#LoadRaster(TmpDir+'outputclip.tif','Resultat')
